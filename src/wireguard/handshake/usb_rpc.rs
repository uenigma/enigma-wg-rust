// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 */

// USB
use serialport::{ SerialPortSettings, StopBits, DataBits, FlowControl, Parity};
use generic_array::GenericArray;

use generic_array::typenum::U32;
use std::convert::TryInto;
use std::time::{SystemTime};
use std::io::ErrorKind;

use crc::{crc32, Hasher32};
use std::error::Error;
use std::fmt::Display;
use std::fmt;

pub fn create_crc32( content : Vec<u8>) -> u32
{
    return crc32::checksum_ieee((content.as_slice()));
}

static mut MSG_ID_COUNTER: u16 = 0x1;
//Structure for RPC packet

pub struct RpcPacket
{
    msg_id      : u16,
    msg_len     : u16,
    body_len    : u8,
    msg_offset  : u8,
    command     : u16,

    spare       : u32,
    crc32       : u32,
    body        : [u8; 48]
}



enum RPC_COMMAND
{
    USER_AUTH 				= 0x0010,
    USER_AUTH_CHANGE		= 0x0011,

    KEY_SAVE 				= 0x0020,
    KEY_GENERATE			= 0x0021,
    KEY_DELETE				= 0x0022,
    KEY_GET_PUBKEY			= 0x0023,

    TIME_READ				= 0x0030,
    TIME_SET				= 0x0031,

    WG_GET_AEAD				= 0x0040,

    //Error codes
    ERROR_NOT_AUTHENTICATED = 0xF010,
    ERROR_WRONG_PASSWORD	= 0xF011,

    ERROR_KEY_READ 			= 0xF020,
    ERROR_KEY_WRITE 		= 0xF021,

    //GENERAL
    ERROR_COMMAND_NOT_FOUND = 0xF0F0,
    ERROR_CRC32_CHECK		= 0xF0F1
}

#[derive(Debug)]
struct RpcError
{
    code : u16
}

impl RpcError {
    fn new(code : u16) -> RpcError {
        RpcError{code}
    }
}

impl fmt::Display for RpcError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"{}", RPC_COMMAND::from_u16_to_string(self.code))
    }
}

impl Error for RpcError {
    fn description(&self) -> &str {
        &RPC_COMMAND::from_u16_to_string(self.code)
    }
}



impl RPC_COMMAND
{
    fn from_u16( input : u16) -> RPC_COMMAND
    {
        let mut cmd = RPC_COMMAND::ERROR_COMMAND_NOT_FOUND;

        if input == RPC_COMMAND::USER_AUTH as u16
        {cmd = RPC_COMMAND::USER_AUTH}
        else if input == RPC_COMMAND::USER_AUTH_CHANGE as u16
        {cmd = RPC_COMMAND::USER_AUTH_CHANGE}

        else if input == RPC_COMMAND::KEY_SAVE as u16
        {cmd = RPC_COMMAND::KEY_SAVE}
        else if input == RPC_COMMAND::KEY_GENERATE as u16
        {cmd = RPC_COMMAND::KEY_GENERATE}
        else if input == RPC_COMMAND::KEY_DELETE as u16
        {cmd = RPC_COMMAND::KEY_DELETE}
        else if input == RPC_COMMAND::KEY_GET_PUBKEY as u16
        {cmd = RPC_COMMAND::KEY_GET_PUBKEY}

        else if input == RPC_COMMAND::TIME_READ as u16
        {cmd = RPC_COMMAND::TIME_READ}
        else if input == RPC_COMMAND::TIME_SET as u16
        {cmd = RPC_COMMAND::TIME_SET}

        else if input == RPC_COMMAND::WG_GET_AEAD as u16
        {cmd = RPC_COMMAND::WG_GET_AEAD}

        else if input == RPC_COMMAND::ERROR_NOT_AUTHENTICATED as u16
        {cmd = RPC_COMMAND::ERROR_NOT_AUTHENTICATED}
        else if input == RPC_COMMAND::ERROR_WRONG_PASSWORD as u16
        {cmd = RPC_COMMAND::ERROR_WRONG_PASSWORD}

        else if input == RPC_COMMAND::ERROR_KEY_READ as u16
        {cmd = RPC_COMMAND::ERROR_KEY_READ}
        else if input == RPC_COMMAND::ERROR_KEY_WRITE as u16
        {cmd = RPC_COMMAND::ERROR_KEY_WRITE}

        return cmd;
    }

    fn from_u16_to_string ( input : u16) -> &'static str
    {
        if input == RPC_COMMAND::USER_AUTH as u16
        {return &"authentication successful"}
        else if input == RPC_COMMAND::USER_AUTH_CHANGE as u16
        {return &"password successfully changed"}

        else if input == RPC_COMMAND::KEY_SAVE as u16
        {return &("key successfully saved")}
        else if input == RPC_COMMAND::KEY_GENERATE as u16
        {return &("key successfully generated")}
        else if input == RPC_COMMAND::KEY_DELETE as u16
        {return &("key successfully deleted")}
        else if input == RPC_COMMAND::KEY_GET_PUBKEY as u16
        {return &("public key successfully obtained")}

        else if input == RPC_COMMAND::TIME_READ as u16
        {return &("time successfully read")}
        else if input == RPC_COMMAND::TIME_SET as u16
        {return &("time successfully set")}

        else if input == RPC_COMMAND::WG_GET_AEAD as u16
        {return &("aead successfully obtained")}

        else if input == RPC_COMMAND::ERROR_NOT_AUTHENTICATED as u16
        {return &("not authenticated")}
        else if input == RPC_COMMAND::ERROR_WRONG_PASSWORD as u16
        {return &("wrong password")}

        else if input == RPC_COMMAND::ERROR_KEY_READ as u16
        {return &("could not obtain key")}
        else if input == RPC_COMMAND::ERROR_KEY_WRITE as u16
        {return &("could not save key")}
        else if input == RPC_COMMAND::ERROR_CRC32_CHECK as u16
        {return &("CRC32 check failed")}


        return &("Unknown Command")

    }

    pub fn is_error( input : u16 ) -> bool
    {
        return     input == 0x000 || input == RPC_COMMAND::ERROR_COMMAND_NOT_FOUND as u16

            || input == RPC_COMMAND::ERROR_NOT_AUTHENTICATED as u16
            || input == RPC_COMMAND::ERROR_WRONG_PASSWORD as u16
            || input == RPC_COMMAND::ERROR_KEY_READ as u16
            || input == RPC_COMMAND::ERROR_KEY_WRITE as u16
    }
}

// assign a "constructor"
impl RpcPacket
{
    const RPC_BODY_SIZE : usize = 48;
    const RPC_HEADER_SIZE : usize = 16;
    const RPC_PACKET_SIZE : usize = RpcPacket::RPC_HEADER_SIZE + RpcPacket::RPC_BODY_SIZE;

    fn transform_u64_to_array_of_u8_big_endian(x:u64) -> [u8;8] {
        let b1 : u8 = ((x >> 56) & 0xff) as u8;
        let b2 : u8 = ((x >> 48) & 0xff) as u8;
        let b3 : u8 = ((x >> 40) & 0xff) as u8;
        let b4 : u8 = ((x >> 32) & 0xff) as u8;
        let b5 : u8 = ((x >> 24) & 0xff) as u8;
        let b6 : u8 = ((x >> 16) & 0xff) as u8;
        let b7 : u8 = ((x >> 8) & 0xff) as u8;
        let b8 : u8 = (x & 0xff) as u8;
        return [b8, b7, b6, b5, b4, b3, b2, b1]
    }

    pub fn transform_u32_to_array_of_u8_big_endian(x:u32) -> [u8;4] {
        let b1 : u8 = ((x >> 24) & 0xff) as u8;
        let b2 : u8 = ((x >> 16) & 0xff) as u8;
        let b3 : u8 = ((x >> 8) & 0xff) as u8;
        let b4 : u8 = (x & 0xff) as u8;
        return [b4, b3, b2, b1]
    }

    pub fn transform_u16_to_array_of_u8_big_endian(x:u16) -> [u8;2] {
        let b1 : u8 = ((x >> 8) & 0xff) as u8;
        let b2 : u8 = (x & 0xff) as u8;
        return [b2, b1]
    }

    pub fn transform_array_of_u8_to_u16(x: &[u8; 2], is_big_endian : bool) -> u16 {
        return if is_big_endian {
            ((x[0] as u16) <<  8) +
            ((x[1] as u16) <<  0)
        }
        else
        {
           ((x[0] as u16) <<  0) +
           ((x[1] as u16) <<  8)
        }
    }

    pub fn transform_array_of_u8_to_u32_big_endian(x: &[u8; 4], is_big_endian : bool) -> u32 {
        return if is_big_endian {
                ((x[0] as u32) << 24) +
                ((x[1] as u32) << 16) +
                ((x[2] as u32) << 8) +
                ((x[3] as u32) << 0)
        }
        else
        {
                ((x[0] as u32) << 0) +
                ((x[1] as u32) << 8) +
                ((x[2] as u32) << 16) +
                ((x[3] as u32) << 24)
        }
    }

    pub fn transform_array_of_u8_to_u64_big_endian(x: &[u8; 8], is_big_endian : bool) -> u64 {
        return if is_big_endian {
            ((x[0] as u64) << 56) +
            ((x[1] as u64) << 48) +
            ((x[2] as u64) << 40) +
            ((x[3] as u64) << 32) +
            ((x[4] as u64) << 24) +
            ((x[5] as u64) << 16) +
            ((x[6] as u64) <<  8) +
            ((x[7] as u64) <<  0)
        }
        else
        {
                ((x[0] as u64) << 0) +
                ((x[1] as u64) << 8) +
                ((x[2] as u64) << 16) +
                ((x[3] as u64) << 24) +
                ((x[4] as u64) << 32) +
                ((x[5] as u64) << 40) +
                ((x[6] as u64) << 48) +
                ((x[7] as u64) << 56)
        }
    }


    pub fn get_msg_id(do_increment : bool) -> u16
    {
        if do_increment { RpcPacket::increment_msg_id(); }
        unsafe {return MSG_ID_COUNTER.clone()};
    }

    pub fn increment_msg_id()
    {
        unsafe { MSG_ID_COUNTER +=1};
    }

    pub fn new(use_same_msgid : bool, msg_len : u16, body_len : u8, msg_offset : u8, command : u16, body : &[u8; 48]) -> RpcPacket {

        let mut pkt =  RpcPacket {
            msg_id : if use_same_msgid { RpcPacket::get_msg_id(false)} else { RpcPacket::get_msg_id(true)},
            msg_len : msg_len,
            body_len : body_len,
            msg_offset : msg_offset,
            command : command,
            spare : 0x00000000,
            crc32 : 0x00000000,
            body : body.clone()
        };
        pkt.crc32 = create_crc32(pkt.to_array().to_vec());
        return pkt;
    }
    pub fn new_from_response(response : [u8; RpcPacket::RPC_PACKET_SIZE] ) -> RpcPacket
    {

        let mut body = [0; RpcPacket::RPC_BODY_SIZE];
        body.copy_from_slice(&response[16..(RpcPacket::RPC_PACKET_SIZE)]);

        return RpcPacket {
            msg_id : RpcPacket::transform_array_of_u8_to_u16(&response[0..2].try_into().expect("parsing error"), false),
            msg_len : RpcPacket::transform_array_of_u8_to_u16(&response[2..4].try_into().expect("parsing error"), false),
            body_len : response[4],
            msg_offset : response[5],
            command : RpcPacket::transform_array_of_u8_to_u16(&response[6..8].try_into().expect("parsing error"), false),
            spare : RpcPacket::transform_array_of_u8_to_u32_big_endian(&response[8..12].try_into().expect("parsing error"), false),
            crc32 : RpcPacket::transform_array_of_u8_to_u32_big_endian(&response[12..16].try_into().expect("parsing error"), false),
            body : body
        }


    }

    pub fn crc32_check(&mut self) -> bool
    {
        let msg_crc = self.crc32;
        self.crc32 = 0x00000000; // set to zero
        let crc = create_crc32(self.to_array().to_vec());
        self.crc32 = msg_crc; // give it back
        if crc != msg_crc {
            return false;
        }
        return true;

    }

    pub fn get_body(&self) -> [u8; RpcPacket::RPC_BODY_SIZE]
    {
        return self.body;
    }

    pub fn to_array(&self) -> [u8; RpcPacket::RPC_PACKET_SIZE]
    {
        let trans_msg_id = RpcPacket::transform_u16_to_array_of_u8_big_endian(self.msg_id);
        let trans_msg_len = RpcPacket::transform_u16_to_array_of_u8_big_endian(self.msg_len);
        let trans_command = RpcPacket::transform_u16_to_array_of_u8_big_endian(self.command);
        let trans_spare = RpcPacket::transform_u32_to_array_of_u8_big_endian(self.spare);
        let trans_crc32 = RpcPacket::transform_u32_to_array_of_u8_big_endian(self.crc32);

        let ret_header = [
            trans_msg_id[0], trans_msg_id[1],
            trans_msg_len[0],trans_msg_len[1],
            self.body_len,
            self.msg_offset,
            trans_command[0], trans_command[1],
            trans_spare[0],trans_spare[1],trans_spare[2],trans_spare[3],
            trans_crc32[0],trans_crc32[1],trans_crc32[2],trans_crc32[3]];

        let mut ret_arr: [u8; RpcPacket::RPC_PACKET_SIZE] = [0; RpcPacket::RPC_PACKET_SIZE];

        for (i, elem) in ret_header.iter().enumerate() {
            ret_arr[i] = *elem;
        }
        for (i, elem) in self.body.iter().enumerate() {
            ret_arr[i + RpcPacket::RPC_HEADER_SIZE ] = *elem;
        }
        return ret_arr
    }
}

fn create_usb_rpc_messages( payload:Vec<u8>, command : u16 ) -> Vec<RpcPacket>
{
    let mut packets: Vec<RpcPacket> = Vec::new();
    let amount_packets  = (payload.len() / RpcPacket::RPC_PACKET_SIZE) + 1;
    let size_of_message = payload.len();

    for i in 0..amount_packets
    {
        let mut current_body_buffer: [u8; RpcPacket::RPC_BODY_SIZE] = [0; RpcPacket::RPC_BODY_SIZE];
        let current_body_size = if size_of_message - (i * RpcPacket::RPC_BODY_SIZE) > RpcPacket::RPC_BODY_SIZE { RpcPacket::RPC_BODY_SIZE} else {size_of_message - (i * RpcPacket::RPC_BODY_SIZE)};
        // Fill the current_body_buffer
        for j in 0 .. RpcPacket::RPC_BODY_SIZE
        {
            if j < current_body_size
            {
                current_body_buffer[j] = payload[i * RpcPacket::RPC_BODY_SIZE + j]
            }
        }

        packets.push(RpcPacket::new(
            true,
            size_of_message as u16,
            current_body_size as u8,
            (i) as u8,
            command,
            &current_body_buffer));
    }
    RpcPacket::increment_msg_id(); // do not forget to call this, when the messages are created
    return packets
}

/**
* Writes a payload "send_vec" to the serial device
* Returns the answer
*/
fn transceive_port(send_vec : Vec<RpcPacket>) -> Result<Vec<RpcPacket>, Box<Error>>
{
    let sending_closure = |send_vec : Vec<RpcPacket>| -> Result< Vec<RpcPacket>, Box<Error>>
        {
            use serialport::open;
            use std::io::Write;
            use std::time::Duration;

            let sp_setting = SerialPortSettings{
                baud_rate: 115200,
                data_bits: DataBits::Five,
                flow_control: FlowControl::None,
                parity: Parity::None,
                stop_bits: StopBits::One,
                timeout: Duration::new(1,0) // 1 sec timeout
            };

            //init port
            let mut port = open("/dev/ttyACM0").expect("Failed to open serial port");
            port.set_all(&sp_setting).expect("Error setting settings to serialport");
            port.set_timeout(Duration::from_millis(1000)).expect("Error setting timeout on serialport");;

            loop
            {
                if port.read_data_set_ready().is_ok() {break;}
                std::thread::sleep(Duration::from_millis(500));
                break; // TODO: Catch error
            }

            //create msg buffer
            for rpc_pkt in send_vec.iter()
            {
                let send_arr = rpc_pkt.to_array();
                //println!("Sending...{:x?}", send_arr.iter());
                //write to port
                port.write(&send_arr).expect("Error writing Serialport");
                //println!("Write Port: {:x?}",send_arr.iter());
            }

            port.set_timeout(Duration::from_millis(1000)).expect("Could not create timeout");

            //Read
            //read into buffer
            let mut curr_packet:[u8; RpcPacket::RPC_PACKET_SIZE] = [0; RpcPacket::RPC_PACKET_SIZE];
            let mut concat_recv_vec: Vec<RpcPacket> = Vec::new();
            loop {
                match port.read(&mut curr_packet)
                {
                    Ok(n) =>
                        {
                            if n == 0
                            {return Ok(concat_recv_vec);}
                            concat_recv_vec.push(RpcPacket::new_from_response(curr_packet));
                            //println!("Received: {:x?}", concat_recv_vec.last().unwrap().to_array().iter() );
                            //println!("CRC32: 0x{:X?}", concat_recv_vec.last().unwrap().crc32 );
                        }
                    Err(e) =>
                        {
                            if e.kind() != ErrorKind::TimedOut
                            {
                                println!("Serialport error: {}",e);
                            }
                            return Ok(concat_recv_vec);
                        }

                }
            }
        };

    /**
    Execute closure and check for "high-level" (RPC) errors and crc32 validity
    */

    let mut retries = 0;
    match sending_closure(send_vec)
    {
        Ok(resp) =>
            {
                let mut concat_recv_vec = resp;
                //CRC32 Check
                for item in &mut concat_recv_vec
                {
                    let status = item.command;
                    //if CRC32 fails
                    if !item.crc32_check() || status == RPC_COMMAND::ERROR_CRC32_CHECK as u16
                    {
                        if  retries < 2
                        {
                            retries+=1;
                            println!("CRC32 check failed, retrying...");
                            // TODO: retry
                        }
                        return Err(Box::new(RpcError::new(RPC_COMMAND::ERROR_CRC32_CHECK as u16)))
                    }
                    if RPC_COMMAND::is_error(status)
                    {
                        return Err(Box::new(RpcError::new(status as u16)))
                    }
                }
                return Ok(concat_recv_vec);
            }
        Err(err) =>
            {
                return Err(err)
            }
    };

}

/** Public RPC functions
*/

/**     PUBLIC
*       Get an AEAD calculated from the device.
*/
pub fn get_aead(public_key:&[u8], c : &[u8], hs : &[u8] ) -> (GenericArray<u8, U32>, [u8;28] ) {

    //TODO make unavailable when not authenticated successfully
    if authenticate(&['1' as u8, '2' as u8,'3' as u8,'4' as u8]) // password is "1234"
        {
             transmit_current_epoch();
        }
    //create msg buffer
    let send_vec = create_usb_rpc_messages(([public_key.to_vec(), c.to_vec(), hs.to_vec()].concat()).clone(), RPC_COMMAND::WG_GET_AEAD as u16);
    match transceive_port(send_vec)
    {
        Ok(resp) =>
            {
                let concat_recv_vec = resp;

                let mut aead = [0; 28];
                let mut kdf2_ck = [0;32];

                //Parse from receoved packets
                if concat_recv_vec.len() == 2
                {
                    aead.copy_from_slice(&concat_recv_vec[0].get_body()[0..28]);
                    kdf2_ck.copy_from_slice( &[&concat_recv_vec[0].get_body()[28..48].to_vec()[..],&concat_recv_vec[1].get_body()[0..12].to_vec()[..]].concat() );
                }
                println!("ADDDEBUG: USB: AEAD {:x?}", aead);
                println!("ADDDEBUG: USB: t1 {:x?}", kdf2_ck);


                return (GenericArray::clone_from_slice(&kdf2_ck), aead);
            }

        Err(err) =>
            {
                //TODO Handle error
                /*panic()*/
                let mut aead = [0; 28];
                let mut kdf2_ck = [0;32];
                return (GenericArray::clone_from_slice(&kdf2_ck), aead);

            }
    };

}

/**     PUBLIC
*       Authenticate yourself with the device
*/
pub fn authenticate(password:&[u8] ) -> bool
{
    let send_vec = create_usb_rpc_messages(password.to_vec(), RPC_COMMAND::USER_AUTH as u16);
    match transceive_port(send_vec)
    {
     Ok(resp) => {
         let concat_recv_vec = resp;
         let mut err = String::from("");
         if concat_recv_vec.len() == 1
         {
             // Authentication succeeded if body contains 1 and the command is "user authentication"
             if  concat_recv_vec[0].msg_id == concat_recv_vec[0].msg_id
                 && concat_recv_vec[0].command == RPC_COMMAND::USER_AUTH as u16
             {
                 println!("Authentication succeeded");
                 return true;
             }
             println!("Authentication failure: {:?}",RPC_COMMAND::from_u16_to_string(concat_recv_vec[0].command ));
         }
     },
     Err(err) =>
     {
         println!("Error: {}", err.to_string());
         return false;
     }
    }


    return false;
}

/** AUTHENTICATED ONLY
* Transmits the current date to the USB device ( check, if the USB device can run RTC on its own)
*/
pub fn transmit_current_epoch() -> bool
{
    let epoch = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).expect("Error while creating unix epoch");
    let epoch_seconds = epoch.as_secs();
    let send_vec = create_usb_rpc_messages(RpcPacket::transform_u64_to_array_of_u8_big_endian(epoch_seconds).to_vec(), RPC_COMMAND::TIME_SET as u16);
    match transceive_port(send_vec)
    {
        Ok(resp) =>
            {
                let concat_recv_vec = resp;
                let mut err = String::from("");
                if concat_recv_vec.len() == 1
                {
                    // Time set succeeded if body contains 1 and the command is correct
                    if  concat_recv_vec[0].msg_id == concat_recv_vec[0].msg_id
                        && concat_recv_vec[0].command == RPC_COMMAND::TIME_SET as u16
                    {
                        println!("Set timestamp successfully");
                        return true;
                    }
                }

                println!("Error while setting timestamp: {:?}",RPC_COMMAND::from_u16_to_string(concat_recv_vec[0].command ));
                return false;
            }
        Err(err) =>
            {
                panic!(err.to_string());
            }
    };

}